#!/usr/bin/env python3
import sys


def run(lines):
	ptr = 0
	tape = [0 for _ in range(256)]
	commands = []
	for line in lines:
		line = line.strip()
		# skip empty
		if not line:
			continue
		# remove comments
		line = line.split('#')[0]
		commands.extend(line.split(' '))
	count = 1
	for i, cmd in enumerate(commands):
		if cmd.isdecimal():
			count = int(cmd)
		else:
			for _ in range(count):
				# decrement cell pointer
				if cmd == 'gul':
					ptr -= 1
					if ptr < 0:
						ptr = len(tape) - 1
				# increment cell pointer
				elif cmd == 'asch':
					ptr += 1
					if ptr >= len(tape):
						ptr = 0
				# decrement cell
				elif cmd == 'GUL':
					tape[ptr] -= 1
				# increment cell
				elif cmd == 'ASCH':
					tape[ptr] += 1
				# print tape, starting from position 65 in ASCII alphabet
				elif cmd == 'GULASCH':
					asciitape = [chr(t + ord('A') - 1) for t in tape if t > 0]
					print(''.join(asciitape))
				# print tape, starting from positon 97 in ASCII alphabet
				elif cmd == 'gulasch':
					asciitape = [chr(t + ord('a') - 1) for t in tape if t > 0]
					print(''.join(asciitape))
				# print tape, starting from position 48 in ASCII alphabet
				elif cmd == 'g00lasch':
					asciitape = [chr(t + ord('0') - 1) for t in tape if t > 0]
					print(''.join(asciitape))
				# print tape, but raw ASCII
				elif cmd == 'g00l45ch':
					asciitape = [chr(t) for t in tape if t >= 0]
					print(''.join(asciitape))
				# print cell as integer
				elif cmd == 'g00':
					print(tape[ptr])
				else:
					print('du hast das kommando', cmd, 'verwendet das existiert aber nicht')
			count = 1


def usage(arg0):
	print('gulasch benutzt man so:')
	print(arg0, 'dateiname.gulasch')
	print('wobei dateiname.gulasch nur ein platzhalter ist den du selbst ausfüllen musst')


def main():
	if len(sys.argv) < 2:
		usage(sys.argv[0])
		exit()
	filename = sys.argv[1]
	with open(filename, 'r') as f:
		text = f.read()
	lines = text.splitlines()
	run(lines)


if __name__ == '__main__':
	main()
